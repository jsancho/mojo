# -*- coding: utf-8 -*-
##############################################################################
#
#    mojo, a Python library for implementing document based databases
#    Copyright (C) 2013-2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import dbutils
import connection
import MySQLdb

SQL_FIELD_TYPES = {
    'char': 'VARCHAR',
    'text': 'LONGTEXT',
    'float': 'DOUBLE',
    }


class Query(dbutils.Query):
    def sql(self):
        res = "SELECT "
        res += ",".join(["(%s)" % f.sql() for f in self.fields])

        res += " FROM "
        res += ",".join([t.sql() for t in self.tables])

        if self.constraints:
            res += " WHERE "
            res += " AND ".join(["(%s)" % c.sql() for c in self.constraints])

        return res


class Field(dbutils.Field):
    def sql(self):
        return "%s.`%s`" % (self.table.sql(), self.field_name)


class Table(dbutils.Table):
    def sql(self):
        return "`%s`.`%s`" % (self.db_name, self.table_name)


class Constraint(dbutils.Constraint):
    def sql(self):
        operator = self.operator.strip().lower()
        if operator == "starts":
            return "(%s) LIKE (%s)" % (self.args[0].sql(), self.args[1].sql()[:-1] + "%'")
        elif operator == "in":
            return "(%s) IN (%s)" % (self.args[0].sql(), ",".join(["(%s)" % a.sql() for a in self.args[1:]]))
        elif operator == "=":
            return "(%s) = (%s)" % (self.args[0].sql(), self.args[1].sql())
        else:
            token = " %s " % operator.upper()
            return token.join(["(%s)" % a.sql() for a in self.args])


class Literal(dbutils.Literal):
    def sql(self):
        if type(self.value) in (int, float):
            return "%s" % self.value
        else:
            return "'%s'" % str(self.value).replace("'", "''")


class Connection(connection.Connection):
    Query = Query
    Field = Field
    Table = Table
    Constraint = Constraint
    Literal = Literal

    def __init__(self, host="localhost", user="", passwd="", *args, **kwargs):
        self._db_con = MySQLdb.connect(host=host, user=user, passwd=passwd)
        self._db_con_autocommit = MySQLdb.connect(host=host, user=user, passwd=passwd)
        super(Connection, self).__init__(*args, **kwargs)

    def query(self, sql, db=None):
        if db is None:
            db = self._db_con
        cur = db.cursor()
        cur.execute(sql)
        res = cur.fetchall()
        cur.close()
        cur = None
        return res

    def execute(self, sql, db=None):
        if db is None:
            db = self._db_con
        cur = db.cursor()
        res = cur.execute(sql)
        cur.close()
        cur = None
        return res

    def _get_databases(self):
        return [x[0] for x in self.query("SHOW DATABASES")]

    def _get_tables(self, db_name):
        return [x[0] for x in self.query("SHOW TABLES FROM `%s`" % db_name)]

    def _count_rows(self, db_name, table_name):
        return self.query("SELECT COUNT(*) FROM `%s`.`%s`" % (db_name, table_name))[0][0]

    def _create_database(self, db_name):
        return (self.execute("CREATE DATABASE `%s`" % db_name, db=self._db_con_autocommit) or False) and True

    def _get_sql_field_type(self, field_type):
        return SQL_FIELD_TYPES.get(field_type, "UNKNOW")

    def _create_table(self, db_name, table_name, fields):
        primary = []
        sql = "CREATE TABLE `%s`.`%s` (" % (db_name, table_name)

        sql_fields = []
        for f in fields:
            sql_field = "%s %s" % (f['name'], self._get_sql_field_type(f['type']))
            if f.get('size'):
                sql_field += "(%s)" % f['size']
            if f.get('primary'):
                primary.append(f['name'])
            if 'null' in f and not f['null']:
                sql_field += " NOT NULL"
            sql_fields.append(sql_field)
        sql += ",".join(sql_fields)

        if primary:
            sql += ", PRIMARY KEY(%s)" % ",".join(primary)

        sql += ")"

        return (self.execute(sql, db=self._db_con_autocommit) or False) and True

    def _get_cursor(self, query):
        cur = self._db_con.cursor()
        cur.execute(query.sql())
        return cur

    def _next(self, cur):
        return cur.fetchone()

    def _insert(self, db_name, table_name, values):
        keys = []
        vals = []
        for k, v in values.iteritems():
            keys.append(k)
            if type(v) is str:
                vals.append("'%s'" % v.replace("'", "''"))
            else:
                vals.append(str(v))
        sql = "INSERT INTO `%s`.`%s`(%s) VALUES (%s)" % (db_name, table_name, ",".join(keys), ",".join(vals))
        return self.execute(sql)

    def commit(self):
        self._db_con.commit()

    def rollback(self):
        self._db_con.rollback()

    def savepoint(self, name):
        self.execute("SAVEPOINT %s" % name)
        return True

    def commit_savepoint(self, name):
        self.execute("RELEASE SAVEPOINT %s" % name)
        return True

    def rollback_savepoint(self, name):
        self.execute("ROLLBACK TO %s" % name)
        return True
