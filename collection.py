# -*- coding: utf-8 -*-
##############################################################################
#
#    mojo, a Python library for implementing document based databases
#    Copyright (C) 2013-2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from cursor import Cursor
from objectid import ObjectId

class Collection(object):
    def __init__(self, database, table_name):
        self.database = database
        self.table_name = str(table_name)

    def __repr__(self):
        return "Collection(%r, %r)" % (self.database, self.table_name)

    def exists(self):
        return (self.database.exists() and self.table_name in self.database.collection_names())

    def _create_field(self, field_name):
        fields = [
            {'name': 'id', 'type': 'char', 'size': 512, 'primary': True},
            {'name': 'name', 'type': 'char', 'size': 64, 'primary': True},
            {'name': 'value', 'type': 'text', 'null': False},
            {'name': 'number', 'type': 'float'},
            ]
        return self.database.connection._create_table(self.database.db_name, '%s$%s' % (self.table_name, field_name), fields)

    def _get_fields(self):
        tables = self.database.connection._get_tables(self.database.db_name)
        return [str(x[x.find('$')+1:]) for x in filter(lambda x: x.startswith('%s$' % self.table_name), tables)]

    def count(self):
        return self.database.connection._count(self.database.db_name, self.table_name)

    def find(self, *args, **kwargs):
        return Cursor(self, *args, **kwargs)

    def insert(self, doc_or_docs):
        if not self.database.db_name in self.database.connection.database_names():
            self.database._create_database()
        if not self.table_name in self.database.collection_names():
            self._create_field('_id')

        if not type(doc_or_docs) in (list, tuple):
            docs = [doc_or_docs]
        else:
            docs = doc_or_docs
        for doc in docs:
            doc_id = str(ObjectId())
            if not '_id' in doc:
                doc['_id'] = doc_id
            self._insert_document(doc_id, doc)

        if type(doc_or_docs) in (list, tuple):
            return [d['_id'] for d in docs]
        else:
            return docs[0]['_id']

    def _insert_document(self, doc_id, doc):
        fields = self._get_fields()
        self.database.connection.savepoint("insert_document")
        try:
            for f in doc:
                if not f in fields:
                    self._create_field(f)
                table_f = '%s$%s' % (self.table_name, f)
                self._insert_field(doc_id, table_f, f, doc[f])
            self.database.connection.commit_savepoint("insert_document")
        except:
            self.database.connection.rollback_savepoint("insert_document")
            raise

    def _insert_field(self, doc_id, field_table, field_name, field_value):
        values = {
            'id': doc_id,
            'name': field_name,
            'value': self.database.connection.serializer.dumps(field_value),
            }
        if type(field_value) in (int, float):
            values['number'] = field_value

        self.database.connection._insert(self.database.db_name, field_table, values)

        if type(field_value) in (list, tuple) and not '.' in field_name:
            for i in xrange(len(field_value)):
                self._insert_field(doc_id, field_table, "%s..%s" % (field_name, i), field_value[i])
        elif type(field_value) is dict:
            for k, v in field_value.iteritems():
                self._insert_field(doc_id, field_table, "%s.%s" % (field_name, k), v)
