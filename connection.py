# -*- coding: utf-8 -*-
##############################################################################
#
#    mojo, a Python library for implementing document based databases
#    Copyright (C) 2013-2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from database import Database
import dbutils

class Connection(object):
    Query = dbutils.Query
    Field = dbutils.Field
    Table = dbutils.Table
    Constraint = dbutils.Constraint
    Literal = dbutils.Literal

    def __init__(self, serializer=None, *args, **kwargs):
        if serializer is None:
            import msgpack
            self.serializer = msgpack
        else:
            self.serializer = serializer

    def __getattr__(self, db_name):
        return Database(self, db_name)

    def __getitem__(self, *args, **kwargs):
        return self.__getattr__(*args, **kwargs)

    def __repr__(self):
        return "Connection(%s)" % self._db_con

    def _get_databases(self):
        return []

    def database_names(self):
        try:
            return [str(x) for x in self._get_databases()]
        except:
            return []

    def _get_tables(self, db_name):
        return []

    def collection_names(self, db_name):
        try:
            return list(set([str(x.split('$')[0]) for x in filter(lambda x: '$' in x, self._get_tables(db_name))]))
        except:
            return []

    def _count_rows(self, db_name, table_name):
        return 0

    def _count(self, db_name, table_name):
        try:
            return self._count_rows(db_name, table_name + '$_id')
        except:
            return 0

    def _create_database(self, db_name):
        return None

    def _create_table(self, db_name, table_name, fields):
        # [{'name': 'id', 'type': 'char', 'size': 20, 'primary': True}]
        return None

    def _get_cursor(self, query):
        return None

    def _next(self, db_name, cursor):
        return None

    def _insert(self, db_name, table_name, values):
        return None

    def commit(self):
        pass

    def rollback(self):
        pass

    def savepoint(self, name):
        pass

    def commit_savepoint(self, name):
        pass

    def rollback_savepoint(self, name):
        pass
