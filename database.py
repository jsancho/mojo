# -*- coding: utf-8 -*-
##############################################################################
#
#    mojo, a Python library for implementing document based databases
#    Copyright (C) 2013-2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from collection import Collection

class Database(object):
    def __init__(self, connection, db_name):
        self.connection = connection
        self.db_name = str(db_name)

    def __getattr__(self, table_name):
        return Collection(self, table_name)

    def __getitem__(self, *args, **kwargs):
        return self.__getattr__(*args, **kwargs)

    def __repr__(self):
        return "Database(%r, %r)" % (self.connection, self.db_name)

    def _create_database(self):
        return self.connection._create_database(self.db_name)

    def exists(self):
        return (self.db_name in self.connection.database_names())

    def collection_names(self):
        return self.connection.collection_names(self.db_name)
