# -*- coding: utf-8 -*-
##############################################################################
#
#    mojo, a Python library for implementing document based databases
#    Copyright (C) 2013-2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class SQLGeneric(object):
    def sql(self):
        return ""


class Query(SQLGeneric):
    def __init__(self, fields, tables, constraints):
        self.fields = fields
        self.tables = tables
        self.constraints = constraints


class Field(SQLGeneric):
    def __init__(self, table, field_name):
        self.table = table
        self.field_name = field_name


class Table(SQLGeneric):
    def __init__(self, db_name, table_name):
        self.db_name = db_name
        self.table_name = table_name


class Constraint(SQLGeneric):
    def __init__(self, operator, *args):
        self.operator = operator
        self.args = args

class Literal(SQLGeneric):
    def __init__(self, value):
        self.value = value
