# -*- coding: utf-8 -*-
##############################################################################
#
#    mojo, a Python library for implementing document based databases
#    Copyright (C) 2013-2014 by Javier Sancho Fernandez <jsf at jsancho dot org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import mojo.MySQL
import unittest

class TestSequenceFunctions(unittest.TestCase):
    def setUp(self):
        self.cmojo = mojo.MySQL.Connection(host="localhost", user="mojo")

    def test_simple_transaction(self):
        self.cmojo.test.t1.insert({'a': 'this is a test', 'b': [1, 2, 3]})
        self.assertEqual(self.cmojo.test.t1.count(), 1)
        self.cmojo.rollback()
        self.assertEqual(self.cmojo.test.t1.count(), 0)

    def test_saving_lists(self):
        doc_id = self.cmojo.test.t1.insert({'a': [1, 2, 3]})
        self.assertEqual(self.cmojo.test.t1.find({'a': 2}).next()['_id'], doc_id)
        self.assertEqual(self.cmojo.test.t1.find({'a': [1, 2, 3]}).next()['_id'], doc_id)

    def test_saving_dictionaries(self):
        doc_id = self.cmojo.test.t1.insert({'a': {'b': {'c': 10}}})
        self.assertEqual(self.cmojo.test.t1.find({'a': {'b': {'c': 10}}}).next()['_id'], doc_id)
        self.assertEqual(self.cmojo.test.t1.find({'a.b': {'c': 10}}).next()['_id'], doc_id)
        self.assertEqual(self.cmojo.test.t1.find({'a.b.c': 10}).next()['_id'], doc_id)

    def tearDown(self):
        self.cmojo.rollback()

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSequenceFunctions)
    unittest.TextTestRunner(verbosity=2).run(suite)
